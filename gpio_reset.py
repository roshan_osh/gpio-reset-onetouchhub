#!/usr/bin/python

# Scady Pty Ltd.
#
# @category    OneTouchHub
# @package     OneTouchHub_GPIO_REST
# @author      Scady Team <hello@scady.io>
# @copyright   Copyright (c) 2018 Scady Pty Ltd. (http://scady.io)

from flask import Flask, jsonify, request
import RPi.GPIO as GPIO

app = Flask(__name__)

VALID_BCM_PIN_NUMBERS_OUTPUT = [6, 9, 10, 11, 13, 17, 19, 22, 26, 27]
VALID_BCM_PIN_NUMBERS_INPUT = [20, 7, 24, 23, 21]
VALID_HIGH_VALUES = [1, '1', 'HIGH']
VALID_LOW_VALUES = [0, '0', 'LOW']
PIN_NAMES = {
             '6': 'Relay 4',
             '9': 'IR5',
             '10': 'IR 4',
             '11': 'IR 6',
             '13': 'relay 1 ',
             '17': 'IR 3',
             '19': 'relay 2 ',
             '22': 'IR 1',
             '26': 'relay 3',
             '27': 'IR 2',
             '20': 'input 1',
             '7': 'input 2',
             '24': 'input 3',
             '23': 'input 4',
             '21': 'wifi reset',
             }

GPIO.setmode(GPIO.BCM)

for pin in VALID_BCM_PIN_NUMBERS_OUTPUT:
    GPIO.setup(pin, GPIO.OUT)


for pin in VALID_BCM_PIN_NUMBERS_INPUT:
    GPIO.setup(pin, GPIO.IN)


def pin_status(pin_number: object) -> object:
    if (pin_number in VALID_BCM_PIN_NUMBERS_OUTPUT) or (pin_number in VALID_BCM_PIN_NUMBERS_INPUT):
        value = GPIO.input(pin_number)
        data = {'pin_number': pin_number,
                'pin_name': PIN_NAMES[str(pin_number)],
                'value': value,
                'status': 'SUCCESS',
                'error': None}
    else:
        data = {'status': 'ERROR',
                'error': 'Invalid pin number.'}

    return data


def pin_update(pin_number: object, value: object) -> object:
    if pin_number in VALID_BCM_PIN_NUMBERS_OUTPUT:
        GPIO.output(pin_number, value)
        new_value = GPIO.input(pin_number)
        data = {'status': 'SUCCESS',
                'error': None,
                'pin_number': pin_number,
                'pin_name': PIN_NAMES[str(pin_number)],
                'new_value': new_value}
    else:
        data = {'status': 'ERROR',
                'error': 'Invalid pin number or value.'}

    return data


@app.route("/api/v1/ping/", methods=['GET'])
def api_status() -> object:
    if request.method == 'GET':
        data = {'api_name': 'RPi GPIO API',
                'version': '1.0',
                'status': 'SUCCESS',
                'response': 'pong'}
        return jsonify(data)


@app.route("/api/v1/gpio/<pin_number>/", methods=['POST', 'GET'])
def gpio_pin(pin_number) -> object:
    pin_number = int(pin_number)
    data = None
    if request.method == 'GET':
        data = pin_status(pin_number)

    elif request.method == 'POST':
        value = request.values['value']
        if value in VALID_HIGH_VALUES:
            data = pin_update(pin_number, 1)
        elif value in VALID_LOW_VALUES:
            data = pin_update(pin_number, 0)
        else:
            data = {'status': 'ERROR',
                    'error': 'Invalid value.'}
    return jsonify(data)


@app.route("/api/v1/gpio/status/", methods=['GET'])
def gpio_status() -> object:
    data_list = []
    VALID_BCM_PIN_NUMBERS = VALID_BCM_PIN_NUMBERS_OUTPUT + VALID_BCM_PIN_NUMBERS_INPUT;
    for pin in VALID_BCM_PIN_NUMBERS:
        data_list.append(pin_status(pin))

    data = {'data': data_list}
    return jsonify(data)


@app.route("/api/v1/gpio/all-high/", methods=['POST'])
def gpio_all_high() -> object:
    data_list = []
    for pin in VALID_BCM_PIN_NUMBERS_OUTPUT:
        data_list.append(pin_update(pin, 1))

    data = {'data': data_list}
    return jsonify(data)


@app.route("/api/v1/gpio/all-low/", methods=['POST'])
def gpio_all_low() -> object:
    data_list = []
    for pin in VALID_BCM_PIN_NUMBERS_OUTPUT:
        data_list.append(pin_update(pin, 0))

    data = {'data': data_list}
    return jsonify(data)


if __name__ == "__main__":
    app.run(debug=True, host='192.168.1.3', port=5001)
